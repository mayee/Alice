var DefaultTableManaged = function () {
    var config;
    var initTable = function () {
        var oTable = $('#def_manage_table');
        oTable.dataTable({
            "language": {
                url: config.language
            },
            "pagingType": config.pagingType,
            "sort":false,
            "bLengthChange": eval((config.bLengthChange).toLowerCase()),
            "bServerSide": eval((config.bServerSide).toLowerCase()),
            "sAjaxSource": config.sAjaxSource,
            "bStateSave": eval((config.bStateSave).toLowerCase()),
            "pageLength": config.pageLength//,
            //"aoColumnDefs": [
            ////    //{ "sWidth": "24", "aTargets": [ 0 ] },
            //    {"bSortable": false, "aTargets": [0]}
            ////    //{ "bVisible": false, "aTargets": [ 0 ] }
            //]
        });

        $("#reload").click(function () {
            var el = $(this).closest(".portlet").children(".portlet-body");
            Metronic.blockUI({
                target: el,
                animate: true,
                overlayColor: 'none'
            });
            oTable.fnDraw();
            Metronic.unblockUI(el);
        });
    }

    return {

        //main function to initiate the module
        init: function (s) {
            if (!jQuery().dataTable) {
                return;
            }
            config = eval('(' + s + ')');;
            initTable();
        }

    };

}();