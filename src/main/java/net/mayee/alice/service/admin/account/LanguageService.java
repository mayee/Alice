package net.mayee.alice.service.admin.account;

import net.mayee.alice.dao.admin.account.LanguageDao;
import net.mayee.alice.entity.admin.account.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Transactional
@Service("LanguageService")
public class LanguageService {

    @Autowired
    private LanguageDao languageDao;

    public Language getLanguage(int id) {
        return languageDao.get(id);
    }

    public List<Language> getAll() {
        return languageDao.getAll();
    }

    public void insertLanguage(Language lag) {
        languageDao.insert(lag);
    }
    
    public void updateLanguage(Language lag) {
        languageDao.update(lag);
    }

    public void removeLanguage(String id) {
        languageDao.remove(id);
    }
    
    public int count(Map<String, Object> parameters) {
        return languageDao.count(parameters);
    }
    
    public List<Language> searchByLimit(Map<String, Object> parameters) {
        return languageDao.searchByLimit(parameters);
    }



}
