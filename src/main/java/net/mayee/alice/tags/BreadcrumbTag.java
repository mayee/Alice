package net.mayee.alice.tags;

import jetbrick.template.JetAnnotations;
import jetbrick.template.runtime.JetTagContext;
import net.mayee.alice.entity.menu.sidebar.SidebarMenu;
import net.mayee.alice.functions.base.BaseFunctions;
import net.mayee.common.MenuHelper;

import java.io.IOException;

/**
 * Created by mayee on 15/9/25.
 */
@JetAnnotations.Tags
public class BreadcrumbTag {

    public static void printBreadcrumb(JetTagContext ctx, String levelThreeId) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("<div class=\"page-bar\">");
        sb.append("<ul class=\"page-breadcrumb\">");

        /* level 1 menu */
        SidebarMenu sbm = MenuHelper.getInstance().getActiveSidebarMenu();
        if (sbm != null && sbm.isActive()) {

            sb.append("<li><i class=\"").append(sbm.getIcon()).append("\"></i> ")
                    .append("<a href=\"");
            if (sbm.isHadSubMenu()) {
                sb.append("javascript:;");
            } else {
                sb.append(BaseFunctions.getPath() + sbm.getHref());
            }
            sb.append("\">")
                    .append(BaseFunctions.getMessage(sbm.getMessageKey(), sbm.getName()))
                    .append("</a>");
        }

        /* level 2 menu */
        SidebarMenu sub_sbm = MenuHelper.getInstance().getActiveSidebarSubMenu();
        if (sub_sbm != null && sub_sbm.isActive() && sbm != null && sbm.isHadSubMenu()) {
            sb.append(" <i class=\"fa fa-angle-right\"></i></li>")
                    .append("<li><a href=\"")
                    .append(BaseFunctions.getPath() + sub_sbm.getHref())
                    .append("\">")
                    .append(BaseFunctions.getMessage(sub_sbm.getMessageKey(), sub_sbm.getName()))
                    .append("</a></li>");
        }

        sb.append("</ul>");
        sb.append("</div>");
        ctx.getWriter().print(sb.toString());
    }
}
