package net.mayee.alice.tags.base;

import java.io.IOException;

import net.mayee.alice.functions.base.BaseFunctions;

import org.apache.commons.lang3.StringUtils;

import jetbrick.template.JetAnnotations;
import jetbrick.template.runtime.JetTagContext;

@JetAnnotations.Tags
public class BaseTags {

//    private static final String DEF_DESCRIPTION = "Alice admin template.";
//    private static String coreCssStr;
//    private static String coreJsStr;
//    
//    static{
//        init();
//    }
//    
//    private static void init(){
//        String path = BaseFunctions.getPath();
//        
//        StringBuilder sb = new StringBuilder();
//        sb.append("<link href=\""+path+"/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/bootstrap-responsive.min.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/style-metro.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/style.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/style-responsive.css\" rel=\"stylesheet\" type=\"text/css\" />")
//        .append("<link href=\""+path+"/css/default.css\" rel=\"stylesheet\" type=\"text/css\" id=\"style_color\" />")
//        .append("<link href=\""+path+"/css/uniform.default.css\" rel=\"stylesheet\" type=\"text/css\" />");
//        coreCssStr = sb.toString();
//        
//        sb = new StringBuilder();
//        sb.append("<script src=\""+path+"/js/jquery-1.10.1.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/jquery-migrate-1.2.1.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/jquery-ui-1.10.1.custom.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/bootstrap.min.js\" type=\"text/javascript\"></script>")
//        .append("<!--[if lt IE 9]>")
//        .append("<script src=\""+path+"/js/excanvas.min.js\"></script>")
//        .append("<script src=\""+path+"/js/respond.min.js\"></script>")
//        .append("<![endif]-->")
//        .append("<script src=\""+path+"/js/jquery.slimscroll.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/jquery.blockui.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/jquery.cookie.min.js\" type=\"text/javascript\"></script>")
//        .append("<script src=\""+path+"/js/jquery.uniform.min.js\" type=\"text/javascript\"></script>");
//        coreJsStr = sb.toString();
//    }
//    
//    public static void getMeta(JetTagContext ctx, String description) throws IOException {
//        if(StringUtils.isBlank(description))
//            description = DEF_DESCRIPTION;
//        StringBuilder sb = new StringBuilder("<meta charset=\"utf-8\" />");
//        sb.append("<meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\" />")
//          .append("<meta content=\"")
//          .append(description)
//          .append("\" name=\"description\" />")
//          .append("<meta content=\"mayee\" name=\"author\" />");
//        ctx.getWriter().print(sb.toString());
//    }
//    
//    public static void getCoreCss(JetTagContext ctx) throws IOException {
//        if(StringUtils.isEmpty(coreCssStr))
//            init();
//        ctx.getWriter().print(coreCssStr);
//    }
//    
//    public static void getCoreJs(JetTagContext ctx) throws IOException {
//        if(StringUtils.isEmpty(coreJsStr))
//            init();
//        ctx.getWriter().print(coreJsStr);
//    }
//
//    public static void getIcon(JetTagContext ctx) throws IOException {
//        String path = BaseFunctions.getPath();
//        StringBuilder sb = new StringBuilder("<link rel=\"shortcut icon\" href=\""+path+"/image/favicon.ico\" />");
//        ctx.getWriter().print(sb.toString());
//    }
    
}
