package net.mayee.alice.spring.exception;

/**
 * Created by mayee on 16/1/29.
 */
public class AliceException extends RuntimeException {

    public AliceException() {
        super();
    }

    public AliceException(String message) {
        super(message);
    }

    public AliceException(Throwable cause) {
        super(cause);
    }

    public AliceException(String message, Throwable cause) {
        super(message, cause);
    }

}
