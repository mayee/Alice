package net.mayee.alice.spring.shiro;

import jodd.props.Props;
import net.mayee.common.AliceHelper;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * Created by mayee on 15/9/22.
 */
public class AliceHashedCredentialsMatcher extends HashedCredentialsMatcher {

    public AliceHashedCredentialsMatcher(String hashAlgorithmName) {
        super(hashAlgorithmName);
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        Props p = AliceHelper.getInstance().getJoddProps();
        if("true".equals(p.getValue("test.running")) && "false".equals(p.getValue("test.login.password.check"))){
            System.out.println("Test model was open, password check closed!");
            return true;
        }else{
            return super.doCredentialsMatch(token, info);
        }
    }

}
