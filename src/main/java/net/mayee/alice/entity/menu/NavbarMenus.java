package net.mayee.alice.entity.menu;

import net.mayee.alice.entity.menu.nanbar.Logo;
import net.mayee.alice.entity.menu.nanbar.Logout;
import net.mayee.alice.entity.menu.nanbar.Profile;

/**
 * Created by mayee on 15/9/20.
 */
public class NavbarMenus {
    private Logo logo;
    private Profile profile;
    private Logout logout;

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Logout getLogout() {
        return logout;
    }

    public void setLogout(Logout logout) {
        this.logout = logout;
    }
}
