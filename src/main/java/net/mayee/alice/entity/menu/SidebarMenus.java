package net.mayee.alice.entity.menu;

import net.mayee.alice.entity.menu.sidebar.SidebarMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayee on 15/9/20.
 */
public class SidebarMenus {
    private List<SidebarMenu> sidebarMenu = new ArrayList<SidebarMenu>();

    public List<SidebarMenu> getSidebarMenu() {
        return sidebarMenu;
    }

    public void setSidebarMenu(List<SidebarMenu> sidebarMenu) {
        this.sidebarMenu = sidebarMenu;
    }
}
