package net.mayee.alice.entity.menu.sidebar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayee on 15/9/16.
 */
public class SidebarMenu implements Serializable {

    private String id;
    private String name;
    private String messageKey;
    private String href;
    private String icon;
    private boolean isSubMenu = false;
    private boolean active = false;

    private SidebarMenu parentSidebarMenu;
    private List<SidebarMenu> subMenuList = new ArrayList<SidebarMenu>();

    public SidebarMenu(String id, String name, String messageKey) {
        this.id = id;
        this.name = name;
        this.messageKey = messageKey;
    }

    public SidebarMenu(String id, String name, String messageKey, String href, String icon) {
        this.id = id;
        this.name = name;
        this.messageKey = messageKey;
        this.href = href;
        this.icon = icon;
    }

    public SidebarMenu(String id, String name, String messageKey, String href, String icon, boolean isSubMenu) {
        this.id = id;
        this.name = name;
        this.messageKey = messageKey;
        this.href = href;
        this.icon = icon;
        this.isSubMenu = isSubMenu;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isHadSubMenu() {
        return this.getSubMenuList().size() > 0;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public SidebarMenu getParentSidebarMenu() {
        return parentSidebarMenu;
    }

    public void setParentSidebarMenu(SidebarMenu parentSidebarMenu) {
        this.parentSidebarMenu = parentSidebarMenu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public String getHrefNoLine() {
        if(href.startsWith("\\/")){
            return href.substring(1);
        }else{
            return href;
        }
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isSubMenu() {
        return isSubMenu;
    }

    public void setIsSubMenu(boolean isSubMenu) {
        this.isSubMenu = isSubMenu;
    }

    public List<SidebarMenu> getSubMenuList() {
        return subMenuList;
    }

    public void setSubMenuList(List<SidebarMenu> subMenuList) {
        this.subMenuList = subMenuList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SidebarMenu that = (SidebarMenu) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
