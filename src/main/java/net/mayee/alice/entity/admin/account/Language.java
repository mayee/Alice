package net.mayee.alice.entity.admin.account;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Language implements Serializable {

	private static final long serialVersionUID = 8462135847048260390L;
	private Integer id;
	private String code;
	private String name;

    public Language() {
    }

    public Language(Integer id, String code, String name) {
		this.id = id;
		this.code = code;
		this.name = name;
	};

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Language language = (Language) o;

		return !(id != null ? !id.equals(language.id) : language.id != null);

	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
