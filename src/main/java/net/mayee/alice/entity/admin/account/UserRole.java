package net.mayee.alice.entity.admin.account;

import java.io.Serializable;

/**
 * Created by mayee on 15/10/22.
 */
public class UserRole implements Serializable {

    private String id;
    private String roleUuid;
    private String userUuid;

    public UserRole() {
    }

    public UserRole(String roleUuid, String userUuid) {
        this.roleUuid = roleUuid;
        this.userUuid = userUuid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(String roleUuid) {
        this.roleUuid = roleUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRole userRole = (UserRole) o;

        return !(id != null ? !id.equals(userRole.id) : userRole.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
